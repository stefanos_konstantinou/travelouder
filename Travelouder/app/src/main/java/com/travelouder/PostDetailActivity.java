package com.travelouder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.travelouder.data.Post;
import com.travelouder.helper.PostDetailFragment;

import java.util.ArrayList;
import java.util.List;

public class PostDetailActivity extends AppCompatActivity {

    private ViewPager viewPager;

    private ArrayList<Post> postList;
    private int currentPostIndex;

    private TextView postIndexCount;
    private TextView textViewTitle;
    private TextView textViewTimestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        Bundle data = getIntent().getExtras();
        postList = data.getParcelableArrayList(Post.PARCEL_TAG);
        currentPostIndex = data.getInt(Post.POST_INDEX);

        viewPager = (ViewPager) findViewById(R.id.postDetailsViewPager);
        viewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(), postList));
        viewPager.setCurrentItem(currentPostIndex);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {
                updateTexts(position);
            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });



        postIndexCount = (TextView) findViewById(R.id.postIndexCount);
        textViewTitle = (TextView) findViewById(R.id.postDetailsTitle);
        textViewTimestamp = (TextView) findViewById(R.id.postDetailsTimestamp);

        updateTexts(currentPostIndex);

    }

    public void updateTexts(int currentPostIndex){
        postIndexCount.setText(formatCounterText(currentPostIndex, postList.size()));
        textViewTitle.setText(postList.get(currentPostIndex).getCaption());
        textViewTimestamp.setText(postList.get(currentPostIndex).getTimestamp());
    }

    public String formatCounterText(int index, int count) {
        return "" + (index + 1) + " / " + count;
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {
        private List<Post> posts;

        public MyPagerAdapter(FragmentManager fragmentManager, List<Post> postList) {
            super(fragmentManager);
            posts = postList;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return posts.size();
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            return PostDetailFragment.newInstance(posts.get(position));
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }




}
