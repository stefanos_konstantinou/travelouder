package com.travelouder.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelouder.PostDetailActivity;
import com.travelouder.R;
import com.travelouder.auth.PostsDownloader;
import com.travelouder.data.Post;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserProfileFragment extends Fragment implements PostsAdapter.ClickHandler{

    public static final String TAG = UserProfileFragment.class.getSimpleName();

    private PostsAdapter mAdapter;
    private RecyclerView recyclerView;
    private FloatingActionButton addNewPhotoFab;


    PostsDownloader postsDownloader;

    String uid;


    public UserProfileFragment() {
        // Required empty public constructor
    }

    public static UserProfileFragment getInstance(String uid){
        UserProfileFragment userProfileFragment = new UserProfileFragment();
        userProfileFragment.uid = uid;
        return userProfileFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            postsDownloader = (PostsDownloader) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PostsDownloader");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);

        addNewPhotoFab = (FloatingActionButton) view.findViewById(R.id.userProfileFab);
        addNewPhotoFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("FEO2", "fab onclick");
                postsDownloader.uploadNewPhotoRequest();
            }
        });

        mAdapter = new PostsAdapter(getActivity(), this);

        recyclerView = (RecyclerView) view.findViewById(R.id.posts_recycler_view);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        uploadMockData();
        fetchPostsData(uid, mAdapter);

        // Inflate the layout for this fragment
        return view;

    }

    public void fetchPostsData(String uid, PostsAdapter adapter) {
        Log.d("FEO1", "Fetch posts data 1");
        postsDownloader.fetchPosts(uid, adapter);
    }

    @Override
    public void onClick(ArrayList<Post> posts, int currentIndex) {
        if(posts != null && !posts.isEmpty()) {
            Intent i = new Intent(getActivity(), PostDetailActivity.class);
            i.putParcelableArrayListExtra(Post.PARCEL_TAG, posts);
            i.putExtra(Post.POST_INDEX, currentIndex);
            startActivity(i);
        }
    }

    @Override
    public void onLongClick(Post post) {

    }
}
