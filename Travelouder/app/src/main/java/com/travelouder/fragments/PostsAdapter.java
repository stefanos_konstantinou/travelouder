package com.travelouder.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.travelouder.R;
import com.travelouder.data.Post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by feorin on 2017-03-27.
 */

public class PostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<Post> postList;
    private ClickHandler clickHandler;
    private Context context;

    public void updatePosts(List<Post> newPosts) {
        postList = new ArrayList<>();
        postList.addAll(newPosts);
        notifyDataSetChanged();
    }

    public PostsAdapter(Context context, ClickHandler clickHandler) {
        this.clickHandler = clickHandler;
        this.context = context;
    }

    public interface ClickHandler {
        void onClick(ArrayList<Post> posts, int currentIndex);
        void onLongClick(Post post);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);
        ImageViewHolder viewHolder = new ImageViewHolder(view);
        setupClickableViewHolder(view, viewHolder);
        return viewHolder;
    }


    public void setupClickableViewHolder(final View v, final RecyclerView.ViewHolder viewHolder) {
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(postList != null && !postList.isEmpty()) {
                    ArrayList postsArrayList = new ArrayList();
                    postsArrayList.addAll(postList);
                    clickHandler.onClick(postsArrayList, viewHolder.getAdapterPosition());
                }
            }
        });
        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(postList != null && !postList.isEmpty()) {
                    Post post = postList.get(viewHolder.getAdapterPosition());
                    clickHandler.onLongClick(post);
                }
                return true;
            }
        });
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Post post = postList.get(position);
        Log.d("FEO1", "bind: " + position);
        if (post != null) {
            Log.d("FEO1", "1 bind: " + position);
            ImageViewHolder ivh = (ImageViewHolder) holder;
            Log.d("FEO1", "2 bind: " + position + ", url " + post.getUrl());
            Picasso.with(context).load(post.getUrl()).into(ivh.image);
        }
    }


    @Override
    public int getItemCount() {
        if (postList == null)
            return 0;
        return postList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        public ImageViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imageViewPost);
        }
    }


}
