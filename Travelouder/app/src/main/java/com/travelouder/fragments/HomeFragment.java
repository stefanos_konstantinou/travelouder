package com.travelouder.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelouder.PostDetailActivity;
import com.travelouder.R;
import com.travelouder.auth.PostsDownloader;
import com.travelouder.data.Post;

import java.util.ArrayList;

/**
 * Created by feorin on 2017-04-24.
 */

public class HomeFragment extends Fragment implements PostsAdapter.ClickHandler {
    public static final String TAG = HomeFragment.class.getSimpleName();

    private PostsAdapter mAdapter;
    private RecyclerView recyclerView;

    PostsDownloader postsDownloader;
    public HomeFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            postsDownloader = (PostsDownloader) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PostsDownloader");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_home, container, false);
        mAdapter = new PostsAdapter(getActivity(), this);

        recyclerView = (RecyclerView) view.findViewById(R.id.posts_recycler_view);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        // Inflate the layout for this fragment
        postsDownloader.fetchAllPosts(mAdapter);
        return view;

    }

    @Override
    public void onClick(ArrayList<Post> posts, int currentIndex) {
        if(posts != null && !posts.isEmpty()) {
            Intent i = new Intent(getActivity(), PostDetailActivity.class);
            i.putParcelableArrayListExtra(Post.PARCEL_TAG, posts);
            i.putExtra(Post.POST_INDEX, currentIndex);
            startActivity(i);
        }
    }

    @Override
    public void onLongClick(Post post) {

    }
}