package com.travelouder.fragments;

/**
 * Created by feorin on 2017-03-28.
 */

public interface OnPhotoUpload {

    void uploadPhoto(String uid, String caption, String url, String placeLatLng);
}
