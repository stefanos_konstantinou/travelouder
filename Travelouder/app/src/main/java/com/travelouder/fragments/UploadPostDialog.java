package com.travelouder.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.travelouder.R;

/**
 * Created by feorin on 2017-03-28.
 */

public class UploadPostDialog extends DialogFragment{

    private OnPhotoUpload onPhotoUpload;
    private String uid;
    private String url;
    private String placeName;
    private String placeLatLng;

    public UploadPostDialog() {}

    public static UploadPostDialog getInstance(OnPhotoUpload onPhotoUpload, String uid, String url, String placeName, String placeLatLng) {
        UploadPostDialog uploadPostDialog = new UploadPostDialog();
        uploadPostDialog.setOnPhotoUpload(onPhotoUpload);
        uploadPostDialog.setUid(uid);
        uploadPostDialog.setUrl(url);
        uploadPostDialog.setPlaceName(placeName);
        uploadPostDialog.setPlaceLatLng(placeLatLng);
        return uploadPostDialog;
    }

    public void setOnPhotoUpload(OnPhotoUpload onPhotoUpload) {
        this.onPhotoUpload = onPhotoUpload;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPlaceLatLng(String placeLatLng) {
        this.placeLatLng = placeLatLng;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_upload_post, null);

        final TextView textViewPlaceName = (TextView) v.findViewById(R.id.textViewPlaceName);
        final EditText editTextCaption = (EditText) v.findViewById(R.id.editTextUploadPhotoCaption);

        textViewPlaceName.setText(placeName);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(v)
                .setTitle("Set caption")
                // Add action buttons
                .setPositiveButton("Upload", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if(editTextCaption != null && onPhotoUpload != null) {
                            String caption = editTextCaption.getText().toString();
                            if(caption.isEmpty()) {
                                caption = "Default caption";
                            }
                            onPhotoUpload.uploadPhoto(uid, caption, url, placeLatLng);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        UploadPostDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
