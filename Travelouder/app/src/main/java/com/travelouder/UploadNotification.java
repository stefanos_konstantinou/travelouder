package com.travelouder;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

/**
 * Created by feorin on 2017-04-02.
 */

public class UploadNotification {

    public UploadNotification(){
    }

    public static void show(Context context, String ticker, String contentTitle, String contentText, int notificationId, boolean setProgress){

        Notification.Builder nbuild = new Notification.Builder(context)
                .setTicker(ticker).setContentTitle(contentTitle)
                .setContentText(contentText).setAutoCancel(true)
                .setSmallIcon(android.R.drawable.ic_menu_upload);

        // true  - filtering in progress
        // false - filtering finished
        if(setProgress) nbuild.setProgress(100, 100, true);
        else nbuild.setProgress(0, 0, false);

        NotificationManager notifManag = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notifManag.notify(notificationId, nbuild.build());
    }

}