package com.travelouder;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.travelouder.auth.NetConnect;
import com.travelouder.auth.PostsDownloader;
import com.travelouder.auth.UploadPostDetails;
import com.travelouder.data.Post;
import com.travelouder.fragments.HomeFragment;
import com.travelouder.fragments.MapViewFragment;
import com.travelouder.fragments.OnPhotoUpload;
import com.travelouder.fragments.PostsAdapter;
import com.travelouder.fragments.UploadPostDialog;
import com.travelouder.fragments.UserProfileFragment;
import com.travelouder.helper.SQLiteHandler;
import com.travelouder.helper.SessionManager;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.makeText;

public class NavigationHome extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        PostsDownloader,
        OnPhotoUpload{

    private static int RESULT_LOAD_IMAGE = 1;
    private static int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2;

    private TextView txtName;
    private TextView txtEmail;
    private TextView emailbar;
    private TextView nameBar;
    private Button btnLogout;

    private SQLiteHandler db;
    private SessionManager session;

    String name = "";
    String email = "";
    String uid = "";

    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtName = (TextView) findViewById(R.id.name);
        txtEmail = (TextView) findViewById(R.id.email);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Fetching user details from sqlite
        HashMap<String, String> user = db.getUserDetails();

         name = user.get("name");
         email = user.get("email");
        uid = user.get("uid");

        // Displaying the user details on the screen
//        txtName.setText(name);
//        txtEmail.setText(email);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_home, menu);
        nameBar=(TextView)findViewById(R.id.namebar);
        emailbar=(TextView) findViewById(R.id.emailbar);
        nameBar.setText(name);
        emailbar.setText(email);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.logoutFragment) {
            logoutUser();
                }
        else if  (id == R.id.userProfile) {
            setTitle("My Profile");
            currentFragment = UserProfileFragment.getInstance(uid);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, currentFragment, "My Profile");
            fragmentTransaction.commit();
        }
        else if  (id == R.id.homeProfile) {
            setTitle("Home");
            currentFragment = new HomeFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, currentFragment, "Home");
            fragmentTransaction.commit();
        }
        else if  (id == R.id.mapFrag) {
            setTitle("Map");
            currentFragment = new MapViewFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, currentFragment, "Map");
            fragmentTransaction.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logoutUser() {
        session.setLogin(false);

        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(NavigationHome.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void fetchPosts(String uid, final PostsAdapter adapter) {
        Call<List<Post>> call = NetConnect.getProxy().getPosts(uid);
        Log.d("FEO1", "Fetch posts data 2");
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                Log.d("FEO1", "Fetch posts data 3");
                List<Post> posts = response.body();
                Log.d("FEO1", "Fetch posts data 4");
                if(posts != null && adapter != null) {
                    Log.d("FEO1", "Fetch posts data 5: " + posts.size());
                    adapter.updatePosts(posts);
                } else {
                    Log.d("FEO1", "Fetch posts data 6");
                    makeText(NavigationHome.this, "Couldn't update posts", Toast.LENGTH_SHORT).show();
                }

            }


            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.d("FEO1", "Fetch posts data 7");
                Toast.makeText(NavigationHome.this, "Server problem occured during when updating posts", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void uploadNewPhotoRequest() {
        verifyStoragePermissions(this);
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    private String picturePath;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            try {
                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .build(this);
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                // TODO: Handle the error.
            } catch (GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }

        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("FEO11", "Place: " + place.getName() + " latlng: " + place.getLatLng());
                Log.d("FEOO3", "Picture path : " + picturePath);
                UploadPostDialog uploadPostDialog = UploadPostDialog.getInstance(this, uid, picturePath, place.getName().toString(), place.getLatLng().latitude + "," + place.getLatLng().longitude);
                uploadPostDialog.show(getSupportFragmentManager(), "uploadPostDialogTag");

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("FEO11", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }



    }

    @Override
    public void uploadPhoto(final String uid, final String caption, final String uri, final String placeLatLng) {
        Log.d("FEOO4", "----- uploadPhoto -----");
        Log.d("FEOO4", "uid: " + uid);
        Log.d("FEOO4", "caption: " + caption);
        Log.d("FEOO4", "uri: " + uri);
        File uncompressedFile = new File(uri);
        int uncompressedFileSize = Integer.parseInt(String.valueOf(uncompressedFile.length()/1024));

        File file = Utils.saveBitmapToFile(uncompressedFile);

        int fileSize = Integer.parseInt(String.valueOf(file.length()/1024));

        Log.d("FEOO4", "size before compression: " + uncompressedFileSize + ", size after compression: " + fileSize);

        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getMimeType(uri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("picture", file.getName(), requestFile);


        RequestBody captionRequestBody =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, caption);

        RequestBody uidRequestBody =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, uid);

        RequestBody placeLatLngRequestBody =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, placeLatLng);


        // finally, execute the request
        Call<UploadPostDetails> call = NetConnect.getProxy().uploadPost(captionRequestBody, uidRequestBody, placeLatLngRequestBody, body);
        UploadNotification.show(this, "Upload post", "Upload post", "Uploading post: " + caption, 1, true);

        call.enqueue(new Callback<UploadPostDetails>() {
            @Override
            public void onResponse(Call<UploadPostDetails> call,
                                   Response<UploadPostDetails> response) {
                Log.v("FEOO5", "onResponse");
                if(response.body().isError()) {
                    Log.v("FEOO5", "isError true: " + response.body().getError_msg());
                    UploadNotification.show(NavigationHome.this, "Upload post", "Upload post", "Failed to upload post: " + caption, 1, false);
                    showToast("Upload failed");
                } else {
                    Log.v("FEOO5", "upload success");
                    UploadNotification.show(NavigationHome.this, "Upload post", "Upload post", "Successfully uploaded post: " + caption, 1, false);
                    showToast("Upload success");

                }

            }

            @Override
            public void onFailure(Call<UploadPostDetails> call, Throwable t) {
                Log.e("FEOO5", "onFailure: " + t.getMessage());
                showToast("Upload failed");
                UploadNotification.show(NavigationHome.this, "Upload post", "Upload post", "Failed to upload post: " + caption, 1, false);
            }
        });

    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public void showToast(String text){
        if(text!= null && !text.isEmpty()) {
            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void fetchAllPosts(final PostsAdapter adapter) {
        Call<List<Post>> call = NetConnect.getProxy().getPosts();
        Log.d("FEO1", "Fetch posts data 2");
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                Log.d("FEO1", "Fetch posts data 3");
                List<Post> posts = response.body();
                Log.d("FEO1", "Fetch posts data 4");
                if(posts != null && adapter != null) {
                    Log.d("FEO1", "Fetch posts data 5: " + posts.size());
                    adapter.updatePosts(posts);
                } else {
                    Log.d("FEO1", "Fetch posts data 6");
                    makeText(NavigationHome.this, "Couldn't update posts", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.d("FEO1", "Fetch posts data 7");
                Toast.makeText(NavigationHome.this, "Server problem occured during when updating posts", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void fetchPostsForMap() {
        Call<List<Post>> call = NetConnect.getProxy().getPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                List<Post> posts = response.body();
                if(currentFragment != null && currentFragment instanceof MapViewFragment){
                    ((MapViewFragment) currentFragment).updateMap(posts);
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Toast.makeText(NavigationHome.this, "Server problem occured during when updating posts", Toast.LENGTH_SHORT).show();
            }
        });


    }

}
