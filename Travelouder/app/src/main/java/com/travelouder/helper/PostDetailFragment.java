package com.travelouder.helper;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.travelouder.R;
import com.travelouder.data.Post;

/**
 * Created by feorin on 2017-03-27.
 */

public class PostDetailFragment extends Fragment{
    // Store instance variables
    private Post post;
    private static final String POST_TAG = "posttag";

    // newInstance constructor for creating fragment with arguments
    public static PostDetailFragment newInstance(Post post) {
        PostDetailFragment fragmentFirst = new PostDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(POST_TAG, post);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        post = getArguments().getParcelable(POST_TAG);
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.post_detail_layout, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageViewPostDetailLayout);
        Picasso.with(getActivity()).load(post.getUrl()).into(imageView);

        return view;
    }

}
