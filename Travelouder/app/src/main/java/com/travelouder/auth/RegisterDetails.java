package com.travelouder.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by feorin on 2017-03-27.
 */

public class RegisterDetails {

    @SerializedName("error")
    @Expose
    private boolean error = false;

    public boolean isError() {
        return error;
    }

    @SerializedName("error_msg")
    @Expose
    private String error_msg;


    public String getError_msg() {
        return error_msg;
    }

}
