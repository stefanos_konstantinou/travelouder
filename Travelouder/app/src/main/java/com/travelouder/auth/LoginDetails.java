package com.travelouder.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by feorin on 2017-03-27.
 */

public class LoginDetails {

    @SerializedName("error")
    @Expose
    private boolean error = false;

    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("error_msg")
    @Expose
    private String error_msg;

    public String getError_msg() {
        return error_msg;
    }

    public boolean getError() {
        return error;
    }

    public String getUid() {
        return uid;
    }

    public User getUser() {
        return user;
    }


    @Override
    public String toString() {
        return "User : " + getUser().getName();
    }
}
