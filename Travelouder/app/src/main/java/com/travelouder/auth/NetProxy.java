package com.travelouder.auth;

import com.travelouder.data.Post;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by feorin on 2017-03-27.
 */

public interface NetProxy {

    String SERVER = "http://146.176.251.24/android_login_api/";

    String LOGIN = "login.php";
    String REGISTER = "register.php";
    String FETCH_POSTS = "fetchposts.php";
    String UPLOAD_POST = "uploadpost.php";
    String FETCH_ALL="fetchAllposts.php";

    @POST(LOGIN)
    Call<LoginDetails> login(@Header("Email") String email, @Header("Password") String password);

    @POST(REGISTER)
    Call<RegisterDetails> register(@Header("Name") String name, @Header("Email") String email, @Header("Password") String password);

    @GET(FETCH_POSTS)
    Call<List<Post>> getPosts(@Query("UID") String uid);

    @GET(FETCH_ALL)
    Call<List<Post>> getPosts();

    @Multipart
    @POST(UPLOAD_POST)
    Call<UploadPostDetails> uploadPost(@Part("caption") RequestBody caption, @Part("uid") RequestBody uid, @Part("placeLatLng") RequestBody placeLatLng, @Part MultipartBody.Part photo);

}
