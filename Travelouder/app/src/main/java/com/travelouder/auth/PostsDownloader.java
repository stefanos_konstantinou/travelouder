package com.travelouder.auth;

import com.travelouder.fragments.PostsAdapter;

/**
 * Created by feorin on 2017-04-24.
 */

public interface PostsDownloader {
    void fetchPosts(String uid, PostsAdapter adapter);
    void uploadNewPhotoRequest();
    void fetchAllPosts(PostsAdapter adapter);
    void fetchPostsForMap();
}
