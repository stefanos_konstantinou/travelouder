package com.travelouder.auth;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by feorin on 2017-03-28.
 */

public class NetConnect {

    private static NetProxy netProxy;

    public static NetProxy getProxy() {
        if(netProxy == null) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            netProxy = new Retrofit.Builder()
                    .baseUrl(NetProxy.SERVER)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
                    .create(NetProxy.class);
        }


        return netProxy;
    }


}
